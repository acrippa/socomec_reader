# Socomec Reader

A small program to read data from a D50 Socomec control interface with B-30 and I-30 sensors.

In the `assets/channels.py` file you can add as many values to read as you want, the modbus table with the addresses are available on the socomec site, ([DIRIS Digiware I30](https://www.socomec.com/files/live/sites/systemsite/files/SCP/15_nouvellemesure/DIRIS%20Digiware/DIRIS-DIGIWARE-I30-HTML-CLIENT_COMMUNICATION-TABLE_CMT_2018-01_MULTI.html))

In the `config.ini` file you must set the D50 ip address and you can configure the timing of the reading

It send data to a remote API by a POST request
