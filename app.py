#!/usr/bin/env python

import sys
import configparser
from apscheduler.schedulers.blocking import BlockingScheduler
from readings import loads, energies
from synchronize import sync

# =======================
# Platform
# =======================

print("Used Python version: ")
print(sys.version)

# =======================
# Scheduler
# =======================

config = configparser.ConfigParser()
config.read_file(open(r'config.ini'))

sched = BlockingScheduler()

sched.add_job(loads.read, 'cron', 
    second = config.get('Loads', 'second'),
    minute = config.get('Loads', 'minute'),
    hour = config.get('Loads', 'hour'))

sched.add_job(energies.read, 'cron', 
    second = config.get('Energies', 'second'),
    minute = config.get('Energies', 'minute'),
    hour = config.get('Energies', 'hour'))

sched.add_job(sync.synchronize, 'cron', 
    second = config.get('Sync', 'second'),
    minute = config.get('Sync', 'minute'),
    hour = config.get('Sync', 'hour'))

sched.start()