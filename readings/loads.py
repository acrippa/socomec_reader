import datetime
from random import random
from assets import channels
from DB import models
from modbus import socomec

def read():
    reading_time = datetime.datetime.now()
    client = socomec.connect()
    if client:
        for load in channels.listLoads:
            try:
                save(load, reading_time, socomec.read(client, load))
            except Exception as ex:
                print(str(ex))
        client.close()

def save(load, reading_time, value):
    models.Load.create(
        machine_id = load['machine_id'],
        type = load['type'],
        value = value,
        created_at = reading_time,
        updated_at = reading_time
    )