import datetime
import decimal
from random import random
from assets import channels
from DB import models
from modbus import socomec

def read():
    reading_time = datetime.datetime.now()
    client = socomec.connect()
    if client: 
        for energy in channels.listEnergies:
            try:
                save(energy, reading_time, socomec.read(client, energy))
            except Exception as ex:
                print(str(ex))
        client.close()

def save(energy, reading_time, value):
    value = decimal.Decimal(value)
    try:
        last = models.Energy.select().where((models.Energy.machine_id == energy['machine_id']) & (models.Energy.type.contains(energy['type']))).order_by(models.Energy.id.desc()).get()
        total = last.total
    except Exception as ex:
        total = 0

    models.Energy.create(
        machine_id = energy['machine_id'],
        type = energy['type'],
        total = value,
        value = value - total,
        created_at = reading_time,
        updated_at = reading_time
    )