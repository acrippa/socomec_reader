# =======================
# MACHINES
# =======================
listMachines = list()
listMachines.append({'id': 1, 'description': 'B30 Unit', 'unit': 0x0A})
listMachines.append({'id': 2, 'description': 'I30 Unit', 'unit': 0x03})

# =======================
# LOADS
# =======================
listLoads = list()
''' B30 '''
listLoads.append({'machine_id': 1, 'type': "Total_Active_Power", 'address': 18476, 'words': 2, 'unit': 0x0A, 'factor': 1, 'data_type': 'signed'})
''' I30 '''
listLoads.append({'machine_id': 1, 'type': "Total_Active_Power", 'address': 18476, 'words': 2, 'unit': 0x03, 'factor': 1, 'data_type': 'signed'})

# =======================
# ENERGY
# =======================
listEnergies = list()
''' B30 '''
listEnergies.append({'machine_id': 1, 'type': "Total_Positive_Active_Energy", 'address': 19843, 'words': 2, 'unit': 0x0A, 'factor': 1, 'data_type': 'unsigned'})
''' I30 '''
listEnergies.append({'machine_id': 1, 'type': "Total_Positive_Active_Energy", 'address': 19841, 'words': 2, 'unit': 0x03, 'factor': 1, 'data_type': 'unsigned'})
