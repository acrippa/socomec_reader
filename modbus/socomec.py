import configparser
import time
from pymodbus.client.sync import ModbusTcpClient
from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.transaction import ModbusRtuFramer as ModbusFramer

def connect():
    tries = 0
    config = configparser.ConfigParser()
    config.read_file(open(r'config.ini'))
    address = config.get('Socomec', 'address')
    port = config.get('Socomec', 'rtu_port')
    timeout = config.get('Socomec', 'timeout')
    sleeptime = config.get('Socomec', 'sleeptime')
    print("Connecting to ", address)
    socomec = ModbusTcpClient(address, port, framer=ModbusFramer, timeout=int(timeout))
    ret = False
    while ret == False & tries < int(config.get('Socomec', 'retry')):
        tries+= 1
        ret = socomec.connect()
        if ret:
            print("Connected to Socomec.")
            return socomec
        else:
            print("Connection to Socomec failed. Sleep")
            time.sleep(int(sleeptime))
    return False

def read(client, register):
    value = 0
    try:
        handle = client.read_holding_registers(register['address'], register['words'], unit=register['unit'])
        print(handle)
        if register['words'] > 1:
            decoder = BinaryPayloadDecoder.fromRegisters(handle.registers, '>', Endian.Big)
            if register['data_type'] == 'signed':
                value = decoder.decode_32bit_int()/float(register['factor'])
            else:
                value = decoder.decode_32bit_uint()/float(register['factor'])
        else:
            value = handle.registers[0]/float(factor)
    except Exception as ex:
        print(str(ex))
    return value
