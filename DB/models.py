from .connection import db
from peewee import *
from playhouse import shortcuts
import json

class BaseModel(Model):

    def to_dict(self):
        return shortcuts.model_to_dict(self)

    def to_json(self):
        return json.dumps(shortcuts.model_to_dict(self), indent=4, sort_keys=True, default=str)

    class Meta:
        database = db

class Load(BaseModel):
    machine_id = IntegerField()
    type = TextField()
    value = IntegerField()
    created_at = DateTimeField()
    updated_at = DateTimeField()

class Energy(BaseModel):
    machine_id = IntegerField()
    type = TextField()
    total = IntegerField()
    value = IntegerField()
    created_at = DateTimeField()
    updated_at = DateTimeField()

class Sync(BaseModel):
    last_sync = DateTimeField()


db.create_tables([Load, Energy, Sync])
