import configparser
from peewee import *

config = configparser.ConfigParser()
config.read_file(open(r'config.ini'))
db = SqliteDatabase(config.get('DB', 'database'))
db.connect()