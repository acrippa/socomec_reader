import configparser
import datetime
import json
import requests
from DB import models
from assets import constants

def synchronize():
    config = configparser.ConfigParser()
    config.read_file(open(r'config.ini'))
    now = datetime.datetime.now()
    syncs = models.Sync.select()
    if len(syncs) == 1:
        sync = syncs.get()
        last_sync = sync.last_sync
        sync.last_sync = now
    else:
        last_sync = 0
        sync = models.Sync(last_sync = now)
    loads = models.Load.select().where(models.Load.updated_at > last_sync)
    energies = models.Energy.select().where(models.Energy.updated_at > last_sync)
    payload = {}
    payload['API_KEY'] = config.get('API', 'key')
    payload['loads'] = []
    payload['energies'] = []
    
    for load in loads:
        payload['loads'].append(json.loads(load.to_json()))
    
    for energy in energies:
        payload['energies'].append(json.loads(energy.to_json()))

    headers = {'content-type': 'application/json'}
    r = requests.post(config.get('API', 'sync'), json=payload, verify=False, headers=headers)
    if r.text == 'OK':
        sync.save()